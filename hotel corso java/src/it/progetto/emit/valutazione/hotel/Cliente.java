package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity
public class Cliente {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCliente;
	private String nome;
	private String cognome;
	private String partitaIva;
	private String telefono;
	private String mail;
	private String nomeDitta;
	private String indirizzoDitta;
	private String telefonoDitta;
	private String password;
	
	@ManyToOne
	TipologiaCliente tipologiaCliente;
	
	@OneToMany(mappedBy="cliente")
	List<Prenotazione> listaPrenotazioni;
	
	@OneToMany(mappedBy="cliente")
	List<Valutazione> listaValutazioni;
	
	public Cliente() {
		super();
		listaPrenotazioni = new ArrayList<Prenotazione>();
	}
	public void addPrenotazione(Prenotazione p) {
		listaPrenotazioni.add(p);
		p.setCliente(this);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public TipologiaCliente getTipologiaCliente() {
		return tipologiaCliente;
	}

	public void setTipologiaCliente(TipologiaCliente tipologiaCliente) {
		this.tipologiaCliente = tipologiaCliente;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNomeDitta() {
		return nomeDitta;
	}

	public void setNomeDitta(String nomeDitta) {
		this.nomeDitta = nomeDitta;
	}

	public String getIndirizzoDitta() {
		return indirizzoDitta;
	}

	public void setIndirizzoDitta(String indirizzoDitta) {
		this.indirizzoDitta = indirizzoDitta;
	}

	public String getTelefonoDitta() {
		return telefonoDitta;
	}

	public void setTelefonoDitta(String telefonoDitta) {
		this.telefonoDitta = telefonoDitta;
	}
	
	public List<Prenotazione> getPrenotazione() {
		return listaPrenotazioni;
	}

	public void setPrenotazione(List<Prenotazione> listaPrenotazioni) {
		this.listaPrenotazioni = listaPrenotazioni;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public List<Prenotazione> getListaPrenotazioni() {
		return listaPrenotazioni;
	}
	public void setListaPrenotazioni(List<Prenotazione> listaPrenotazioni) {
		this.listaPrenotazioni = listaPrenotazioni;
	}
	public List<Valutazione> getListaValutazioni() {
		return listaValutazioni;
	}
	public void setListaValutazioni(List<Valutazione> listaValutazioni) {
		this.listaValutazioni = listaValutazioni;
	}

}
