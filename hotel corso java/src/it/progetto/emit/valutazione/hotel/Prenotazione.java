package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Prenotazione {

		@Id
		@GeneratedValue(strategy=GenerationType.IDENTITY)
		private int idPrenotazione;
		
		private int numeroPrenotazione=1;
		
		@ManyToOne
		Cliente cliente;
		
		@ManyToOne
		Hotel hotel;
		
		@OneToMany(mappedBy="prenotazione")
		List<Camera> listaCamerePrenotate;
		
		public Prenotazione() {
			super();
			listaCamerePrenotate = new ArrayList<Camera>();
		}
		
		public void addCamera(Camera c) {
			listaCamerePrenotate.add(c);
			c.setPrenotazione(this);
		}
		
		public int getNumeroPrenotazione() {
			return numeroPrenotazione;
		}

		public int setNumeroPrenotazione(int numeroPrenotazione) {
			return this.numeroPrenotazione = numeroPrenotazione;
		}

		public Cliente getCliente() {
			return cliente;
		}

		public void setCliente(Cliente cliente) {
			this.cliente = cliente;
		}

		public List<Camera> getListaCamerePrenotate() {
			return listaCamerePrenotate;
		}

		public void setListaCamerePrenotate(List<Camera> listaCamerePrenotate) {
			this.listaCamerePrenotate = listaCamerePrenotate;
		}

		public int getIdPrenotazione() {
			return idPrenotazione;
		}

		public void setIdPrenotazione(int idPrenotazione) {
			this.idPrenotazione = idPrenotazione;
		}

		public Hotel getHotel() {
			return hotel;
		}

		public void setHotel(Hotel hotel) {
			this.hotel = hotel;
		}
		
}
