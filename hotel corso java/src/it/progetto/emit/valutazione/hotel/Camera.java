package it.progetto.emit.valutazione.hotel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.ManyToOne;

@Entity
public class Camera {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@ManyToOne
	Hotel hotel;
	@ManyToOne
	TipologiaCamera tipologiaCamera;
	
	@ManyToOne
	Prenotazione prenotazione;
	
	private double prezzoCamera;
	private int numeroCamera;
	private String statoCamera = "libera";
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrezzoCamera() {
		return prezzoCamera;
	}

	public void setPrezzoCamera(double prezzoCamera) {
		this.prezzoCamera = prezzoCamera;
	}

	public int getNumeroCamera() {
		return numeroCamera;
	}

	public void setNumeroCamera(int numeroCamera) {
		this.numeroCamera = numeroCamera;
	}

	public String getStatoCamera() {
		return statoCamera;
	}

	public void setStatoCamera(String statoCamera) {
		this.statoCamera = statoCamera;
	}
	
	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	public TipologiaCamera getTipologiaCamera() {
		return tipologiaCamera;
	}

	public void setTipologiaCamera(TipologiaCamera tipologiaCamera) {
		this.tipologiaCamera = tipologiaCamera;
	}

	public Prenotazione getPrenotazione() {
		return prenotazione;
	}

	public void setPrenotazione(Prenotazione prenotazione) {
		this.prenotazione = prenotazione;
	}

}
