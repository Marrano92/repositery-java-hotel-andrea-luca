package it.progetto.emit.valutazione.hotel;

import java.util.Scanner;

import javax.persistence.EntityManager;


public class App {

	static Scanner in = new Scanner(System.in);
	static private EntityManager enMa;

	public static void main(String[] args) {
		enMa = JPAUtils.getEntityManager();
		// GestioneHotel gestione = new GestioneHotel();
		App app = new App();
		app.creazioneListaBaseHotel();
		int imputMenu;
		do {

			app.gestioneMenu();
			imputMenu = in.nextInt();
			in.nextLine();

			switch (imputMenu) {
			case 1:

				break;
			case 2:
				inserimentoHotel();
				app.stampaListaHotel();
			case 3:

				break;
			case 4:

				break;
			case 5:

				break;
				
			default:
				app.gestioneMenu();
			}
		} while (imputMenu != 6);

		enMa.close();
	}

	public void gestioneMenu() {
		System.out.println("Scegli quale servizio utilizzare.");
		System.out.println("1-Prenotazione" + "\n2-\tAggiungere nuovo Hotel" + "\n3-\tValutare."
				+ "\n4-\tVisualizzare Recensioni." + "\n5-\tModifica Hotel esistente." + "\n6-\tEsci.");
	}

	// public void uscitaMenu (){
	// System.out.println("Sei sicuro di uscire?");
	// System.out.println("Si"
	// + "\n2-\tNo");
	// String imputUscita = in.nextLine();
	// if(imputUscita.equalsIgnoreCase("si") ||
	// imputUscita.equalsIgnoreCase("s�"));
	// int uscita=1;
	//
	// }

	private static void inserimentoHotel() {
		System.out.println("Come si chiama l'Hotel?");
		String nome = in.nextLine();
		System.out.println("In che via � situato?");
		String indirizzo = in.nextLine();
		System.out.println("Inserisca il numero di telefono dell'Hotel:");
		String numero = in.nextLine();
		System.out.println("Inserisci la descrizione dell'Hotel: ");
		String descrizione = in.nextLine();
		
		enMa.getTransaction().begin();
		
		Hotel hotel = new Hotel();
		hotel.setNome(nome);
		hotel.setIndirizzo(indirizzo);
		hotel.setTelefono(numero);
		hotel.setDescrizione(descrizione);
		enMa.persist(hotel);
		
		String imputTipoCamera;
		int imputAggiuntaCamera;
		TipologiaCamera tipo = null;
		int i = 0;
		int nCamere=0;
	
		do {
			do {
				System.out.println("Che tipo di camera vuoi creare ");
				imputTipoCamera = in.nextLine().toLowerCase();
				tipo = enMa.find(TipologiaCamera.class, imputTipoCamera);
				if (tipo == null) {
					System.out.println("Il tipo inserito non esiste.");
				} else
					break;

			} while (true);
			
			System.out.println("Quante camere vuoi creare di questo tipo?");
			nCamere = in.nextInt();
			in.nextLine();
			System.out.println("Che prezzo avranno?");
			int imputPrezzo = in.nextInt();
			in.nextLine();

			for (int k = 0; k < nCamere; k++) {
				i++;
				Camera camera = new Camera();
				camera.setNumeroCamera(i);
				camera.setPrezzoCamera(imputPrezzo);
				camera.setTipologiaCamera(tipo);
				hotel.addCamera(camera);
				enMa.persist(camera);
			}

			System.out.println("Camere inserite con successo!");
			
			enMa.getTransaction().commit();

			System.out.println("Vuoi uscire o aggiungere altre Camere?" + "\n-1-Aggiungi" + "\n2-Esci");
			imputAggiuntaCamera = in.nextInt();
			if (imputAggiuntaCamera != 1)
				break;
		} while (true);

		System.out.println("Hotel inserito con successo!");
	}

	public void creazioneListaBaseHotel() {

		// attenzione inserire nuova tipo camera in minuscolo!!!
		enMa.getTransaction().begin();

		TipologiaCamera lusso = new TipologiaCamera();
		lusso.setTipoCamera("lusso");
		enMa.persist(lusso);

		TipologiaCamera normale = new TipologiaCamera();
		normale.setTipoCamera("normale");
		enMa.persist(normale);

		TipologiaCamera economica = new TipologiaCamera();
		economica.setTipoCamera("economica");
		enMa.persist(economica);

		Hotel europa = new Hotel();
		europa.setIdHotel(1);
		europa.setNome("Hotel Europa");
		europa.setIndirizzo("Via dei Gigli,18");
		europa.setTelefono("+39 031 235894");
		europa.setDescrizione("Situato in un palazzo storico nel centro storico di Firenze, a 5 minuti a piedi "
				+ "\ndal Duomo e da Ponte Vecchio, l'Helvetia&Bristol Firenze - Starhotels Collezione offre "
				+ "\ncamere eleganti con mobili d'epoca.");
		// europa.setListaCamere(gestione.creazioneCamere(50, 10, 20, 20));

		enMa.persist(europa);

		Hotel royalPalace = new Hotel();
		royalPalace.setIdHotel(2);
		royalPalace.setNome("Hotel Royal Palace");
		royalPalace.setIndirizzo("Via degli imperatori,1");
		royalPalace.setTelefono("+39 031 458217");
		europa.setDescrizione("Situato in un edificio storico progettato dal Brunelleschi, lungo il fiume Arno, "
				+ "\nl'hotel a 5 stelle The St. Regis Florence offre una vista mozzafiato sul Ponte Vecchio, "
				+ "\nun ristorante premiato con stelle Michelin, un centro benessere, una palestra e "
				+ "\ncamere di lusso con mobili antichi.");
		// royalPalace.setListaCamere(gestione.creazioneCamere(200, 50, 75,
		// 75));

		enMa.persist(royalPalace);

		Hotel vergingetorige = new Hotel();
		vergingetorige.setIdHotel(3);
		vergingetorige.setNome("Hotel Vergingetorige");
		vergingetorige.setIndirizzo("Via dei poveri,99");
		vergingetorige.setTelefono("+39 031 325146");
		vergingetorige.setDescrizione("Situato in un edificio storico");
		// vergingetorige.setListaCamere(gestione.creazioneCamere(100, 20, 50,
		// 30));

		enMa.persist(vergingetorige);

		Hotel miramare = new Hotel();
		miramare.setIdHotel(4);
		miramare.setNome("Hotel Miramare");
		miramare.setIndirizzo("Via lungomare di Rimini,16");
		miramare.setTelefono("+39 031 762419");
		miramare.setDescrizione("Situato in un edificio storico progettato dal Brunelleschi, lungo il fiume Arno, "
				+ "\nl'hotel a 5 stelle The St. Regis Florence offre una vista mozzafiato sul Ponte Vecchio, "
				+ "\nun ristorante premiato con stelle Michelin, un centro benessere, una palestra e "
				+ "\ncamere di lusso con mobili antichi.");
		// miramare.setListaCamere(gestione.creazioneCamere(150, 50, 50, 50));

		enMa.persist(miramare);

		enMa.getTransaction().commit();

	}

	// public void avvia() {
	//
	// do {
	// System.out.println(
	// "Vuole prenotare, valutare o visualizzare valutazioni?\nper uscire dal
	// programma digitare exit.");
	// String c = in.nextLine();
	// switch (c.toLowerCase()) {
	// case ("prenotare"): {
	// Cliente cliente = new Cliente();
	// System.out.println("Inserisca il suo nome:");
	// String nome = in.nextLine();
	// cliente.setNome(nome);
	// System.out.println("Inserisca il suo cognome:");
	// String cognome = in.nextLine();
	// cliente.setCognome(cognome);
	// System.out.println("Desidera prenotare a nome di una ditta?");
	// String risposta = in.nextLine();
	// if (risposta == "si") {
	// System.out.println("Indichi la denominazione della ditta:");
	// String denominazione = in.nextLine();
	// cliente.setDenominazione(denominazione);
	// System.out.println("Inserisca la partita iva:");
	// String piva = in.nextLine();
	// cliente.setPiva(piva);
	// } else {
	// cliente.setDenominazione("cliente privato");
	// cliente.setPiva("cliente privato");
	// }
	// System.out.println("Inserire in numero dell'Hotel in cui intende
	// prenotare:\n");
	// stampaListaHotel();
	// int hotelInQuestione = in.nextInt();
	// in.hasNextLine();
	//
	// System.out.println("Quante camere di tipo lusso intende prenotare?");
	// int camereLusso = in.nextInt();
	// in.nextLine();
	// TipoCamera tipoCamera = TipoCamera.Lusso;
	// prenotaCamere(hotelInQuestione, camereLusso, tipoCamera);
	//
	// System.out.println("Quante camere di tipo normale intende prenotare?");
	// int camereNormale = in.nextInt();
	// in.nextLine();
	// tipoCamera = TipoCamera.Normale;
	// prenotaCamere(hotelInQuestione, camereNormale, tipoCamera);
	//
	// System.out.println("Quante camere di tipo economica intende prenotare?");
	// int camereEconomica = in.nextInt();
	// in.nextLine();
	// tipoCamera = TipoCamera.Economica;
	// prenotaCamere(hotelInQuestione, camereEconomica, tipoCamera);
	//
	// System.out.println("Prenotazione effettuata con successo!\nIl suo numero
	// di prenotazione �:"
	// + listaHotel.get(hotelInQuestione).getNumeroPrenotazione());
	// listaHotel.get(hotelInQuestione).incrementaNumeroPrenotazione();
	// break;
	// }
	//
	// case ("valutare"): {
	// System.out.println("Inserire in numero dell'Hotel che intende
	// valutare:\n");
	// stampaListaHotel();
	// int hotelInQuestione = in.nextInt();
	// in.hasNextLine();
	// System.out.println("Inserisca in numero prenotazione:");
	// int numeroPrenotazione = in.nextInt();
	// in.nextLine();
	// if (listaHotel.get(hotelInQuestione).getNumeroPrenotazione() >=
	// numeroPrenotazione) {
	// System.out.println("Dia un voto da 1 a 10 all'Hotel:");
	// double valutazione = in.nextInt();
	// in.nextLine();
	// System.out.println("Descriva la sua esperienza nell'Hotel:");
	// String descrizione = in.nextLine();
	// Valutazione val = new Valutazione();
	// val.setNumeroPrenotazione(numeroPrenotazione);
	// val.setValutazione(valutazione);
	// val.setDescrizione(descrizione);
	// listaHotel.get(hotelInQuestione).aggiungiValutazione(val);
	// System.out.println("Valutazione effettuata con successo!");
	// break;
	// } else
	// System.out.println("Numero valutazione non valido.");
	// break;
	//
	// }
	// case ("visualizzare"):
	// System.out.println("Inserire in numero dell'Hotel di cui intende
	// visualizzare le recenzioni:\n");
	// stampaListaHotel();
	// int hotelInQuestione = in.nextInt();
	// in.nextLine();
	// for (Valutazione val :
	// listaHotel.get(hotelInQuestione).getListaValutazioni()) {
	// System.out.println("-Numero prenotazione: " + val.getNumeroPrenotazione()
	// +
	//
	// "\nValutazione: " + val.getValutazione() + "\nDescrizione: " +
	// val.getDescrizione());
	// }
	// break;
	// }
	//
	// } while (in.nextLine() != "exit");
	// System.out.println("Grazie per aver utilizzato la nostra applicazione.");
	// in.close();
	//
	// }

	private void stampaListaHotel() {

		for (Hotel hotel : enMa.createQuery("Select hotel From Hotel hotel", Hotel.class).getResultList()) {
			System.out.println("Nome Hotel: " + hotel.getNome() + "\nIndirizzo Hotel: " + hotel.getIndirizzo()
					+ "\nTelefono Hotel: " + hotel.getTelefono() + "\nDescrizione Hotel: " + hotel.getDescrizione());
			System.out.println("");

			System.out.println("Tot. Camere: " + hotel.getListaCamere().size());
			System.out.println("Camere Lusso Disponibili: "
					+ hotel.getListaCamere().stream().filter(c -> c.getStatoCamera().equals("libera"))
							.filter(c -> c.getTipologiaCamera().getTipoCamera().equals("lusso")).count());
			System.out.println("Camere Normali Disponibili: "
					+ hotel.getListaCamere().stream().filter(c -> c.getStatoCamera().equals("libera"))
							.filter(c -> c.getTipologiaCamera().getTipoCamera().equals("normale")).count());
			System.out.println("Camere Economiche Disponibili: "
					+ hotel.getListaCamere().stream().filter(c -> c.getStatoCamera().equals("libera"))
							.filter(c -> c.getTipologiaCamera().getTipoCamera().equals("economica")).count());
			System.out.println("\n");
		}
	}

	// private void prenotaCamere(int hotelInQuestione, int numeroCamere,
	// TipoCamera tipocamera) {
	// int contatore = 0;
	// for (@SuppressWarnings("unused")
	// Camera cam : listaHotel.get(hotelInQuestione).getListaCamere()) {
	// if
	// (listaHotel.get(hotelInQuestione).ottieniCamera(contatore).getTipoCamera()
	// == tipocamera) {
	// if (contatore < numeroCamere) {
	// listaHotel.get(hotelInQuestione).ottieniCamera(contatore).cambiaStatoCamera();
	// contatore++;
	//
	// }

	// }
	// }
	// }
}