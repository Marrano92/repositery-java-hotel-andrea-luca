package it.progetto.emit.valutazione.hotel;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAUtils {

	public static EntityManager getEntityManager() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("Hotel");
		EntityManager enMa = emf.createEntityManager();
		return enMa;
	}
}
