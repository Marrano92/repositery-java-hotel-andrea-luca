package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;


public class GestioneHotel {

	private EntityManager enMa = JPAUtils.getEntityManager();
	Scanner in = new Scanner(System.in);

	public Hotel creazioneHotel() {
		System.out.println("Come si chiama l'Hotel?");
		String nome = in.nextLine();
		System.out.println("In che via � situato?");
		String indirizzo = in.nextLine();
		System.out.println("Inserisca il numero di telefono dell'Hotel:");
		String numero = in.nextLine();
		System.out.println("Inserisci la descrizione dell'Hotel: ");
		String descrizione = in.nextLine();

		enMa.getTransaction().begin();
		Hotel hotel = new Hotel();
		hotel.setNome(nome);
		hotel.setIndirizzo(indirizzo);
		hotel.setTelefono(numero);
		hotel.setDescrizione(descrizione);
		enMa.persist(hotel);
		enMa.getTransaction().commit();
		return hotel;
	}

	public int creazioneCamera(Hotel hotel, int numeroDiPartenza) {
		enMa.getTransaction().begin();
		String imputTipoCamera;
		TipologiaCamera tipo = null;
		int nCamere = 0;

		do {
			System.out.println("Che tipo di camera vuoi creare ");
			imputTipoCamera = in.nextLine().toLowerCase();
			tipo = enMa.find(TipologiaCamera.class, imputTipoCamera);
			if (tipo == null) {
				System.out.println("Il tipo inserito non esiste.");
			} else
				break;

		} while (true);

		System.out.println("Quante camere vuoi creare di questo tipo?");
		nCamere = in.nextInt();
		in.nextLine();
		System.out.println("Che prezzo avranno?");
		int imputPrezzo = in.nextInt();
		in.nextLine();

		for (int k = 0; k < nCamere; k++) {
			numeroDiPartenza++;
			Camera camera = new Camera();
			camera.setNumeroCamera(numeroDiPartenza);
			camera.setPrezzoCamera(imputPrezzo);
			camera.setTipologiaCamera(tipo);
			hotel.addCamera(camera);
			enMa.persist(camera);
		}

		System.out.println("Camere inserite con successo!");
		enMa.getTransaction().commit();
		return numeroDiPartenza;
	}

	public boolean aggiuntaCamera() {
		int imputAggiuntaCamera;
		System.out.println("Vuoi uscire o aggiungere altre Camere?" + "\n-1-Aggiungi" + "\n2-Esci");
		imputAggiuntaCamera = in.nextInt();
		in.nextLine();
		return imputAggiuntaCamera == 1;
	}

	public void stampaListaHotel() {

		for (Hotel hotel : enMa.createQuery("Select hotel From Hotel hotel", Hotel.class).getResultList()) {
			System.out.println("Nome Hotel: " + hotel.getNome() + "\nIndirizzo Hotel: " + hotel.getIndirizzo()
					+ "\nTelefono Hotel: " + hotel.getTelefono() + "\nDescrizione Hotel: " + hotel.getDescrizione());
			System.out.println("");

			System.out.println("Tot. Camere: " + hotel.getListaCamere().size());
			List<Camera> camereLibere = new ArrayList<>();
			hotel.getListaCamere().stream().filter(c -> c.getStatoCamera().equals("libera")).forEach(camereLibere::add);
			System.out.println("Tot. Camere libere: " + camereLibere.size());
			System.out.println("Camere Lusso Disponibili: "
					+ camereLibere.stream().filter(c -> c.getTipologiaCamera().getTipoCamera().equals("lusso")).count());
			System.out.println("Camere Normali Disponibili: "
					+ camereLibere.stream().filter(c -> c.getTipologiaCamera().getTipoCamera().equals("normale")).count());
			System.out.println("Camere Economiche Disponibili: "
					+ camereLibere.stream().filter(c -> c.getTipologiaCamera().getTipoCamera().equals("economica")).count());
			System.out.println("\n");
		}
	}

	public void creazionePopolamentoDatiDatabase(List<Valutazione> listaValutazioni) {

		// attenzione inserire nuova tipo camera in minuscolo!!!
		// Creazione tipi di Camere predefiniti nel database
		enMa.getTransaction().begin();

		TipologiaCamera lusso = new TipologiaCamera();
		lusso.setTipoCamera("lusso");
		enMa.persist(lusso);

		TipologiaCamera normale = new TipologiaCamera();
		normale.setTipoCamera("normale");
		enMa.persist(normale);

		TipologiaCamera economica = new TipologiaCamera();
		economica.setTipoCamera("economica");
		enMa.persist(economica);
		// Creazione TipiClienti predefiniti nel database
		TipologiaCliente privato = new TipologiaCliente();
		privato.setTipoCliente("Privato");
		enMa.persist(privato);
		
		// Creazione Clienti predefiniti nel database
		Cliente cliente = new Cliente();
		cliente.setNome("Andrea");
		cliente.setCognome("Marrano");
		cliente.setTelefono("+39 031 235894");
		cliente.setMail("andrea.marrano@yahoo.com");
		cliente.setPassword("prova1992");
		cliente.setTipologiaCliente(privato);
		enMa.persist(cliente);
		
		// Creazione Hotel predefiniti nel database
		Hotel europa = new Hotel();
		europa.setIdHotel(1);
		europa.setNome("Hotel Europa");
		europa.setIndirizzo("Via dei Gigli,18");
		europa.setTelefono("+39 031 235894");
		europa.setDescrizione("Situato in un palazzo storico nel centro storico di Firenze, a 5 minuti a piedi "
				+ "\ndal Duomo e da Ponte Vecchio, l'Helvetia&Bristol Firenze - Starhotels Collezione offre "
				+ "\ncamere eleganti con mobili d'epoca.");
		enMa.persist(europa);
		
		// Creazione Valutazioni predefiniti nel database
				Valutazione valutazione = new Valutazione();
				valutazione.setCliente(cliente);
				valutazione.setDescrizione("Soggiorno in agosto 2016 all'Hotel Europeo di Pinzolo, reso ancor pi� piacevole da un tempo splendido. "
						+ "Panoramic suite, terzo piano, accogliente, spaziosa, arredamento curato, moderno, essenziale, materiali naturali, solidi, doppi servizi, una vasca idromassaggio. "
						+ "Mezza pensione per poter approfittare delle molte possibilit� offerte dalla posizione di Pinzolo: Val di genova e le sue cascate, ovovia e successiva seggiovia in paese, Madonna di Campiglio, lago di Molveno, per citarne alcune."
						+ "Prima colazione con buona scelta di alternative attraenti, self service ma con supporto cortese ed efficiente del personale (giovane e svelto) per una serie appetitosa di leccornie variate."
						+ "Cucina serale ottima, chef molto valido, ampia scelta di piatti, con ottime proposte di cucina locale e di ingredienti di stagione."
						+ "Carni squisite ma anche pesce cucinato in modo eccellente; porzioni generose, dolci impossibili da non provare!"
						+ "Personale rapido, cortese, efficiente, professionale; tempi di attesa ridotti al minimo, presenza discreta ma sentita della proprietaria, alla quale va il merito di una esperienza da consigliare.");
				valutazione.setHotel(europa);
				valutazione.setVotoValutazione(8.0);
				listaValutazioni.add(valutazione);
				enMa.persist(valutazione);

		// Creazione TipiClienti predefiniti nel database
		TipologiaCliente ditta = new TipologiaCliente();
		ditta.setTipoCliente("Ditta");
		enMa.persist(ditta);
		
		// Creazione Clienti predefiniti nel database
				Cliente cliente1 = new Cliente();
				cliente1.setNome("Mario");
				cliente1.setCognome("Rossi");
				cliente1.setTelefono("+39 031 235894");
				cliente1.setMail("mario.rossi@yahoo.com");
				cliente1.setPassword("azienda89");
				cliente1.setTipologiaCliente(ditta);
				cliente1.setIndirizzoDitta("Via degli Alici,1");
				cliente1.setNomeDitta("Rossi SPA");
				cliente1.setTelefonoDitta("+39 031 234564");
				cliente1.setPartitaIva("74864289765");
				enMa.persist(cliente1);

		// Creazione Hotel predefiniti nel database

		Hotel royalPalace = new Hotel();
		royalPalace.setIdHotel(2);
		royalPalace.setNome("Hotel Royal Palace");
		royalPalace.setIndirizzo("Via degli imperatori,1");
		royalPalace.setTelefono("+39 031 458217");
		europa.setDescrizione("Situato in un edificio storico progettato dal Brunelleschi, lungo il fiume Arno, "
				+ "\nl'hotel a 5 stelle The St. Regis Florence offre una vista mozzafiato sul Ponte Vecchio, "
				+ "\nun ristorante premiato con stelle Michelin, un centro benessere, una palestra e "
				+ "\ncamere di lusso con mobili antichi.");
		enMa.persist(royalPalace);
		
		// Creazione Valutazioni predefiniti nel database
		Valutazione valutazione1 = new Valutazione();
		valutazione1.setCliente(cliente1);
		valutazione1.setDescrizione("Per la 1� volta ho soggiornato in questo Hotel e siamo stati Benissimo: ottima posizione vicinissimo al centro, pulizia eccellente, camere ampie e confortevoli, "
				+ "ottime la professionalit�, la cordialit� e la disponibilit� sia del personale che dei titolari."
				+ "La cucina ottima con piatti molto curati e vari , la colazione molto varia e fresca sia nei dolci ( di loro produzione ) che delle altre cibanze");
		valutazione1.setHotel(royalPalace);
		valutazione1.setVotoValutazione(10.0);
		listaValutazioni.add(valutazione1);
		enMa.persist(valutazione1);
		
		// Creazione Hotel predefiniti nel database
		Hotel vergingetorige = new Hotel();
		vergingetorige.setIdHotel(3);
		vergingetorige.setNome("Hotel Vergingetorige");
		vergingetorige.setIndirizzo("Via dei poveri,99");
		vergingetorige.setTelefono("+39 031 325146");
		vergingetorige.setDescrizione("Situato in un edificio storico");
		enMa.persist(vergingetorige);

		Hotel miramare = new Hotel();
		miramare.setIdHotel(4);
		miramare.setNome("Hotel Miramare");
		miramare.setIndirizzo("Via lungomare di Rimini,16");
		miramare.setTelefono("+39 031 762419");
		miramare.setDescrizione("Situato in un edificio storico progettato dal Brunelleschi, lungo il fiume Arno, "
				+ "\nl'hotel a 5 stelle The St. Regis Florence offre una vista mozzafiato sul Ponte Vecchio, "
				+ "\nun ristorante premiato con stelle Michelin, un centro benessere, una palestra e "
				+ "\ncamere di lusso con mobili antichi.");
		enMa.persist(miramare);

		enMa.getTransaction().commit();

	}

	public void gestioneMenu() {
		System.out.println("Scegli quale servizio utilizzare.");
		System.out.println("1-Prenotazione" + "\n2-\tAggiungere nuovo Hotel" + "\n3-\tLogin."
				+ "\n4-\tCrea Recensioni." + "\n5-\tCrea un nuovo Account."
				+ "\n6-\tAggiungi altre camere ad un'Hotel esistente." + "\n7-\tVisualizza Prenotazione effetuate."+ "\n8-\tStampa Valutazioni effetuate." + "\n9-\tEsci.");
	}

	public TipologiaCliente menuCreazioneCliente() {
		TipologiaCliente tipoClie = null;
		System.out.println("E' un Privato o una Ditta?");
		String inputTipoCliente = in.nextLine().toLowerCase();
		tipoClie = enMa.find(TipologiaCliente.class, inputTipoCliente);

		return tipoClie;
	}

	public Cliente creazioneClientiPrivato(TipologiaCliente tipoClie) {
		System.out.println("Quale � il suo nome?");
		String nome = in.nextLine();
		System.out.println("Quale � il suo cognome?");
		String cognome = in.nextLine();
		System.out.println("Inserisca il suo numero di telefono: ");
		String numero = in.nextLine();
		System.out.println("Inserisci la sua mail: ");
		String mail = in.nextLine();
		System.out.println("Inserisci una password: ");
		String password = in.nextLine();

		enMa.getTransaction().begin();
		Cliente cliente = new Cliente();
		cliente.setNome(nome);
		cliente.setCognome(cognome);
		cliente.setTelefono(numero);
		cliente.setMail(mail);
		cliente.setPassword(password);
		cliente.setTipologiaCliente(tipoClie);
		enMa.persist(cliente);
		enMa.getTransaction().commit();
		System.out.println("Account inserito con successo!");
		return cliente;

	}

	public Cliente creazioneClientiDitta(TipologiaCliente tipoClie) {
		System.out.println("Quale � il suo nome?");
		String nome = in.nextLine();
		System.out.println("Quale � il suo Cognome?");
		String cognome = in.nextLine();
		System.out.println("Inserisca il suo numero di telefono:");
		String numero = in.nextLine();
		System.out.println("Inserisci la sua mail: ");
		String mail = in.nextLine();
		System.out.println("Inserisci una password: ");
		String password = in.nextLine();
		System.out.println("Inserisci il nome della ditta: ");
		String nomeDitta = in.nextLine();
		System.out.println("Inserisci la partita IVA: ");
		String partitaIva = in.nextLine();
		System.out.println("Inserisci l'indirizzo della ditta: ");
		String indirizzoDitta = in.nextLine();
		System.out.println("Inserisci il telefono della ditta: ");
		String telefonoDitta = in.nextLine();

		enMa.getTransaction().begin();
		Cliente cliente = new Cliente();
		cliente.setNome(nome);
		cliente.setCognome(cognome);
		cliente.setTelefono(numero);
		cliente.setMail(mail);
		cliente.setPassword(password);
		cliente.setNomeDitta(nomeDitta);
		cliente.setPartitaIva(partitaIva);
		cliente.setIndirizzoDitta(indirizzoDitta);
		cliente.setTelefonoDitta(telefonoDitta);
		cliente.setTipologiaCliente(tipoClie);
		enMa.persist(cliente);
		enMa.getTransaction().commit();
		System.out.println("Account inserito con successo!");
		return cliente;
	}

	public Hotel sceltaPrenotazioneHotel(){
	Hotel tipoHotel = null;
	System.out.println("In quale Hotel desideri prenotare");
	for (Hotel hotel : enMa.createQuery("Select hotel From Hotel hotel", Hotel.class).getResultList()) {
		System.out.println(hotel.getIdHotel() + "- " + "Nome Hotel: " + hotel.getNome());
	}
	int imputHotelPrenotato = in.nextInt();
	// *************************Ciclo Do-While per id-Hotel
	// *************************************************
	do {
		tipoHotel = enMa.find(Hotel.class, imputHotelPrenotato);
		if (tipoHotel == null)
			System.out.println("L'Hotel inserito non esiste.");
		else
			break;
	} while (true);
	in.nextLine();
	return tipoHotel;
	}
	
	public List<Camera> occupaCamere() {
		
		List<Camera> listaCamereDaPrenotare = new ArrayList<>();
		List<Camera> listaCamerePrenotate = new ArrayList<>();
		String inputTipoCamera;
		TipologiaCamera tipo;

		
		// *************************Ciclo Do-While per Tipo Camera

		do {
			System.out.println("Che tipo di camera intendi prenotare?");
			inputTipoCamera = in.next();
			tipo = enMa.find(TipologiaCamera.class, inputTipoCamera);
			if (tipo == null)
				System.out.println("Il tipo di camera inserito non esiste.");
			else
				break;
		} while (true);
		in.nextLine();

		// **************imput e filtro per visualizzare lista camere
		// disponibili****************************************

		System.out.println("Quante camere vuole prenotare?");
		for (Camera camera : enMa
				.createQuery("Select camera From Camera camera "
						+ " WHERE camera.statoCamera =?1 And camera.tipologiaCamera =?2", Camera.class)
				.setParameter(1, "libera").setParameter(2, tipo).getResultList()) {
			camera.getNumeroCamera();
			listaCamereDaPrenotare.add(camera);
		}
		System.out.println("Camere disponibili: " + listaCamereDaPrenotare.size());
		int imputNumeroCamereDaPrenotare = in.nextInt();
		in.nextLine();

		enMa.getTransaction().begin();

		for (int k = 0; k < imputNumeroCamereDaPrenotare; k++) {
			listaCamereDaPrenotare.get(k).setStatoCamera("occupata");
			listaCamerePrenotate.add(listaCamereDaPrenotare.get(k));
			enMa.persist(listaCamereDaPrenotare.get(k));
		}
		enMa.getTransaction().commit();

		System.out.println("Camere prenotate con successo!");

		// ciclo for per stampare camere prenotate

		// for (Camera camera : enMa.createQuery("Select camera From Camera
		// camera "
		// +" WHERE camera.statoCamera =?1 And
		// camera.PrenotazioneNumeroPrenotazione =?2",
		// Camera.class).setParameter(1, "occupata").setParameter(2, 1) {
		// System.out.println("Camere prenotate: " + "\nNumero camera: " +
		// camera.getNumeroCamera());
		// }
		return listaCamerePrenotate;
	}

	public void creazionePrenotazione(int numeroPrenotazione, Cliente clie, List<Camera> camere, Hotel hotel) {

		enMa.getTransaction().begin();
		Prenotazione prenotazione = new Prenotazione();
		prenotazione.setNumeroPrenotazione(numeroPrenotazione);
		prenotazione.setCliente(clie);
		prenotazione.setHotel(hotel);
		clie.addPrenotazione(prenotazione);
		prenotazione.setListaCamerePrenotate(camere);
		enMa.persist(prenotazione);
		enMa.getTransaction().commit();

		System.out.println("Numero Prenotazione: " + prenotazione.getNumeroPrenotazione() + "\nNome Hotel prenotato: "
				+ prenotazione.getHotel().getNome() + "\nNumero Camere Prenotate: "
				+ prenotazione.getListaCamerePrenotate().size());
		for (int k = 0; k < camere.size(); k++) {
			System.out
					.println("\nLista Camere: " +" " + "\nN. Camera: " + camere.get(k).getNumeroCamera() + " " + "Prezzo Camera: "
							+ camere.get(k).getPrezzoCamera() + " " +"Tipo Camera: " + camere.get(k).getStatoCamera());
		}
	}

	public void stampaAccount() {
		System.out.println("Account non trovato, Prima di prenotare creare il proprio account");
	}

	public List<Valutazione> creaValutazione(Cliente clie,List<Valutazione> listaValutazioni){
		
		Hotel tipoHotel;
		double imputNumeroValutazione;
			System.out.println("Quale Hotel desideri valutare");
			/*Ciclo che stampa solo gli hotel in cui l'utente ha prenotato almeno una camera */
			for (Hotel hotel : enMa.createQuery("Select hotel From Hotel hotel", Hotel.class).getResultList()) {
				System.out.println(hotel.getIdHotel() + "- " + "Nome Hotel: " + hotel.getNome());
			}
			int imputHotelValutato = in.nextInt();
			// *************************Ciclo Do-While per id-Hotel
			// *************************************************
			do {
				tipoHotel = enMa.find(Hotel.class, imputHotelValutato);
				if (tipoHotel == null)
					System.out.println("L'Hotel inserito non esiste.");
				else
					break;
			} while (true);
		in.nextLine();
		
		System.out.println("Inserisca il voto della valutazione");
		System.out.println("Da 1 a 10");
		do {
			imputNumeroValutazione = in.nextDouble();
			if (imputNumeroValutazione < 1 | imputNumeroValutazione > 10 )
				System.out.println("Errore inserimento, selezionare un numero tra 1 e 10");
			else
				break;
		} while (true);
		in.nextLine();
		System.out.println("Inserisca una descrizione");
		String descrizioneValutazione = in.nextLine();
		
		enMa.getTransaction().begin();
		Valutazione valutazione = new Valutazione();
		valutazione.setCliente(clie);
		valutazione.setDescrizione(descrizioneValutazione);
		valutazione.setHotel(tipoHotel);
		valutazione.setVotoValutazione(imputNumeroValutazione);
		listaValutazioni.add(valutazione);
		enMa.persist(valutazione);
		
		enMa.getTransaction().commit();
		
		
		return listaValutazioni;
		
	}
	
	public void stampaListaValutazioni(Cliente clie) {
//		int imputCliente = clie.getIdCliente();
		for (Valutazione valutazione : enMa
				.createQuery("Select valutazione From Valutazione valutazione WHERE valutazione.cliente = ?1",
						Valutazione.class)
				.setParameter(1, clie).getResultList()) {
			System.out.println("Numero Valutazione: " + valutazione.getId()
					+ "\nNome Hotel valutato: " + valutazione.getHotel() + "\nDescrizione valutazione: "
					+ valutazione.getDescrizione());
			System.out.println("");
		}
	}
	
	public Cliente login() {
		Cliente cliente = new Cliente();
		do {
			System.out.println("Immetti il tuo nome");
			String nome = in.nextLine();
			System.out.println("Immetti la tua password");
			String password = in.nextLine();
			try {
				cliente = enMa
						.createQuery("Select c From Cliente c Where c.nome= :nome And c.password = :password",
								Cliente.class)
						.setParameter("nome", nome)
						.setParameter("password", password)
						.getSingleResult();
				break;
			} catch (PersistenceException e) {

				System.out.println("L'utente inserito non esiste.");
			}
		} while (true);

		System.out.println("Benvenuto " + cliente.getNome() + " " + cliente.getCognome());
		return cliente;

	}

	public void stampaListaPrenotazione(Cliente clie) {
		int imputCliente = clie.getIdCliente();
		for (Prenotazione prenotazione : enMa
				.createQuery("Select prenotazione From Prenotazione prenotazione WHERE prenotazione.idCliente = ?1",
						Prenotazione.class)
				.setParameter(1, imputCliente).getResultList()) {
			System.out.println("Numero Prenotazione: " + prenotazione.getNumeroPrenotazione()
					+ "\nNome Hotel prenotato: " + prenotazione.getHotel() + "\nNumero CAmere Prenotate: "
					+ prenotazione.getListaCamerePrenotate().size());
			int imputPrenotazione = prenotazione.getIdPrenotazione();

			for (Camera camera : enMa
					.createQuery("Select camera From Camera camera WHERE camera.idPrenotazione = ?1", Camera.class)
					.setParameter(1, imputPrenotazione).getResultList()) {
				System.out.println("\nLista Camere: " + "\n  N. Camera: " + camera.getNumeroCamera() + "  Prezzo Camera: "
						+ camera.getPrezzoCamera() + "  Tipo Camera: " + camera.getTipologiaCamera().getTipoCamera());
			}
		}
	}

	public boolean aggiuntaPrenotazioneCamera() {
		int imputAggiuntaPrenotazioneCamera;
		System.out.println("Vuoi uscire o prenotare altre Camere?" + "\n-1-Prenota" + "\n2-Esci");
		imputAggiuntaPrenotazioneCamera = in.nextInt();
		return imputAggiuntaPrenotazioneCamera == 1;
	}

	public String aggiuntaAltreCameraHotelEsistente() {

		String inputTipoCamera;
		Hotel tipoHotel = null;
		TipologiaCamera tipo;
		int imputHotelAggiungere = 0;
		int nCamere = 0;

		do {
			System.out.println("A quale Hotel desideri agggiungere una camera?");
			for (Hotel hotel : enMa.createQuery("Select hotel From Hotel hotel", Hotel.class).getResultList()) {
				System.out.println(hotel.getIdHotel() + "- " + "Nome: " + hotel.getNome());
			}
			imputHotelAggiungere = in.nextInt();
			tipoHotel = enMa.find(Hotel.class, imputHotelAggiungere);
			if (tipoHotel == null)
				System.out.println("L'Hotel inserito non esiste.");
			else
				break;
		} while (true);

		do {
			System.out.println("Che tipo di camera vuoi aggiungere?");
			inputTipoCamera = in.next();
			tipo = enMa.find(TipologiaCamera.class, inputTipoCamera);
			if (tipoHotel == null)
				System.out.println("Il tipo di camera inserito non esiste.");
			else
				break;
		} while (true);

		System.out.println("Quante camere vuoi aggiungere di questo tipo?");
		nCamere = in.nextInt();
		in.nextLine();
		System.out.println("Che prezzo avranno?");
		int imputPrezzo = in.nextInt();
		in.nextLine();

		// Hotel hotel = enMa.find(Hotel.class, imputHotelAggiungere);
		// camereTotaliHotel = hotel.getCamera().size();

		Integer camereTotaliHotel = (Integer) enMa
				.createQuery("SELECT MAX(camera.numeroCamera) " + "FROM Camera camera WHERE camera.hotel.id = ?1",
						Integer.class)
				.setParameter(1, imputHotelAggiungere).getSingleResult();

		enMa.getTransaction().begin();
		for (int k = 0; k < nCamere; k++) {
			camereTotaliHotel++;
			Camera camera = new Camera();
			camera.setHotel(tipoHotel);
			camera.setNumeroCamera(camereTotaliHotel);
			camera.setPrezzoCamera(imputPrezzo);
			camera.setTipologiaCamera(tipo);
			tipoHotel.addCamera(camera);
			enMa.persist(camera);
		}

		System.out.println("Camere inserite con successo!");
		enMa.getTransaction().commit();
		return "Camere inserite con successo!";

	}

}
