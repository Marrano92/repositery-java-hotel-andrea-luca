package it.progetto.emit.valutazione.hotel;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

@Entity
public class Valutazione {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idValutazioni;
	@Lob
	String descrizione;
	double votoValutazione;
	
	@ManyToOne
	Hotel hotel;
	
	@ManyToOne
	Cliente cliente;

	public int getId() {
		return idValutazioni;
	}

	public void setId(int id) {
		this.idValutazioni = id;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public double getVotoValutazione() {
		return votoValutazione;
	}

	public void setVotoValutazione(double votoValutazione) {
		this.votoValutazione = votoValutazione;
	}

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
}
