package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
//import javax.persistence.GeneratedValue;
//import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;



@Entity
public class TipologiaCamera {

	@Id
	private String tipoCamera;
	
	@OneToMany(mappedBy="tipologiaCamera")
	List<Camera> listaCamere;
	
	public TipologiaCamera() {
		super();
		listaCamere = new ArrayList<Camera>();
	}
	
	public void addCamera(Camera c) {
		listaCamere.add(c);
		c.setTipologiaCamera(this);
	}

	public String getTipoCamera() {
		return tipoCamera;
	}

	public void setTipoCamera(String tipoCamera) {
		this.tipoCamera = tipoCamera;
	}

	public List<Camera> getListaCamere() {
		return listaCamere;
	}

	public void setListaCamere(List<Camera> listaCamere) {
		this.listaCamere = listaCamere;
	}
	
	
}
