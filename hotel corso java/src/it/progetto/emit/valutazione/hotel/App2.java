package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.persistence.EntityManager;

public class App2 {

	static Scanner in = new Scanner(System.in);
	static private EntityManager enMa;

	public static void main(String[] args) {
		enMa = JPAUtils.getEntityManager();
		GestioneHotel gest = new GestioneHotel();
		List<Valutazione> listaValutazioni = new ArrayList<>();
		Cliente idUtenteEsistente = enMa.find(Cliente.class, 1);
		if (idUtenteEsistente == null){
		gest.creazionePopolamentoDatiDatabase(listaValutazioni);
		} else;
		
		int imputMenu;
		Cliente clie = null;
		
		do {

			gest.gestioneMenu();
			imputMenu = in.nextInt();
			in.nextLine();

			switch (imputMenu) {
			
			/*case 1 che fa partire i metodi per la creazione della prenotazione di un hotel */
			case 1:
				if(clie ==null){
					gest.stampaAccount();
					break;}
				else
				do {
					Hotel hotel = gest.sceltaPrenotazioneHotel();
					List<Camera> camere = gest.occupaCamere();
					Integer numeroPrenotazione = (Integer) enMa.createQuery("SELECT MAX(prenotazione.numeroPrenotazione) "
							+ "FROM Prenotazione prenotazione WHERE prenotazione.hotel = ?1", Integer.class).setParameter(1, camere.get(0).getHotel()).getSingleResult();
					numeroPrenotazione++;
					gest.creazionePrenotazione(numeroPrenotazione,clie,camere, hotel);
					
					} while (gest.aggiuntaPrenotazioneCamera());
				break;
				
			/*case 2 che fa partire i metodi per la creazione dell'hotel con il relativo metodo per creare le camere */	
			case 2:
				Hotel h = gest.creazioneHotel();
				int numeroDiPartenza=0;
				do {
				  numeroDiPartenza= gest.creazioneCamera(h, numeroDiPartenza);
				  
				} while (gest.aggiuntaCamera());
				gest.stampaListaHotel();
				break;
				
			/*case 3 che fa partire il metodo per il login dell'utente */	
			case 3:
				clie = gest.login();
				break;
				
			/*case 4 che fa partire i metodi per creazione delle valutazioni */	
			case 4:
				gest.creaValutazione(clie,listaValutazioni);
				break;
				
			/*case 5 creazione utente per tipo */	
			case 5:
				TipologiaCliente tipoClie = gest.menuCreazioneCliente();
				if(tipoClie.getTipoCliente().equalsIgnoreCase ("Privato")){
					clie = gest.creazioneClientiPrivato(tipoClie);
				}
				else if (tipoClie.getTipoCliente().equalsIgnoreCase ("Ditta"))
					 clie = gest.creazioneClientiDitta(tipoClie);
				break;
			case 6:
				gest.aggiuntaAltreCameraHotelEsistente();
				gest.stampaListaHotel();
				break;
			case 7:
				gest.stampaListaPrenotazione(clie);	
				break;
			case 8:
				gest.stampaListaValutazioni(clie);	
				break;
			}
		} while (imputMenu < 9);

		enMa.close();
	}
}
