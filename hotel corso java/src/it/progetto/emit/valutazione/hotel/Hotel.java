package it.progetto.emit.valutazione.hotel;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;

@Entity
public class Hotel {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@OneToMany(mappedBy="hotel", fetch=FetchType.EAGER)
	List<Camera> listaCamere;
	
	@OneToMany(mappedBy="hotel")
	List<Prenotazione> listaPrenotazioni;
	
	@OneToMany(mappedBy="hotel")
	List<Valutazione> listaValutazioni;
	
	private String nome;
	private String indirizzo;
	private String telefono;
	@Lob
	private String descrizione;
	
	public Hotel() {
		super();
		listaCamere = new ArrayList<Camera>();
	}

	public void addCamera(Camera c) {
		listaCamere.add(c);
		c.setHotel(this);
	}
	
	public int getIdHotel() {
		return id;
	}

	public void setIdHotel(int idHotel) {
		this.id = idHotel;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getIndirizzo() {
		return indirizzo;
	}

	public void setIndirizzo(String indirizzo) {
		this.indirizzo = indirizzo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	@Override
	public String toString() {
		return "Hotel [idHotel=" + id + ", nome=" + nome + ", indirizzo=" + indirizzo + ", telefono=" + telefono + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Camera> getListaCamere() {
		return listaCamere;
	}

	public void setListaCamere(List<Camera> listaCamere) {
		this.listaCamere = listaCamere;
	}

	public List<Prenotazione> getListaPrenotazioni() {
		return listaPrenotazioni;
	}

	public void setListaPrenotazioni(List<Prenotazione> listaPrenotazioni) {
		this.listaPrenotazioni = listaPrenotazioni;
	}

	public List<Valutazione> getListaValutazioni() {
		return listaValutazioni;
	}

	public void setListaValutazioni(List<Valutazione> listaValutazioni) {
		this.listaValutazioni = listaValutazioni;
	}

}
